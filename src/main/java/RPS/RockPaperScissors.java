/*Art Disclaimer: To the best of the authors knowledge the images used in this project
 *are public domain clipart, and are therefore NOT under the authors copyright or 
 *the Apache License.
 * 
 *Code Copyright 2019 J.P.Reilly
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 */


/**Exercise: HomeworkTwenty
* Author: J.Reilly
* Project Purpose: Game of Rock Paper Scissors
* Input: user clicks R/P/C image
* Desired Output:  computers choice (red highlight), user choice (blue highlight) and win/lose/tie status (purple highlight is a tie)
* Variables and Classes:  label, GridPane, scene, stage, HPos Enums, double, DropShadow, Random, ArrayList, RPCitem enum (custom), Image, ImageView.
* Formulas:  n/a
* Description of the main algorithm: Setup controls, setup main window, show main window, respond to user choice/clicks by highlighting
* user choice, computer choice and displaying win/lose/tie label with highlighting matching user win/lose/tie status. Computer choice is reset
* after each user choice and win/lose/tie display so the user can select again to play endlessly.
* Date: 12/04/17
*
* Updated: 11/01/19 to use gradle build scripts
**/
package RPS;

import java.util.*;
import javafx.application.*;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.stage.*;
import javafx.scene.layout.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.*;
import javafx.scene.effect.*;
import javafx.scene.input.*;

public class RockPaperScissors  extends Application{
	//controls/objects the event handlers need to use
	public enum RPCitem { ROCK, PAPER, SCISSORS };
	private RPCitem UserItem;
	private RPCitem ComputerItem;
	private Label lblOutput = new Label("Good Luck...");
	private ArrayList<ImageView> imvItems = new ArrayList<>();
	private Random RandomGen = new Random();
	private DropShadow EffectOne = new DropShadow();
	private DropShadow EffectTwo = new DropShadow();
	private DropShadow EffectThree = new DropShadow();
	private DropShadow EffectOneText = new DropShadow();
	private DropShadow EffectTwoText = new DropShadow();
	private DropShadow EffectThreeText = new DropShadow();
	
	public static void main(String[] args) {
		//Nothing to see here...
		launch(args);
	}

	@Override
	public void start(Stage PrimaryStage){
		//create controls
		GridPane GridPaneOne = new GridPane();
		Image imgRock = new Image("rock.png");
		Image imgPaper = new Image("paper.png");
		Image imgScissors = new Image("scissors.png");
		
		//List of all the images/choices, 0 = Rock, 1 = Paper, 2 = Scissors
		imvItems.addAll(Arrays.asList(new ImageView(imgRock), new ImageView(imgPaper), new ImageView(imgScissors)));
		
		//generate initial computer value
		ComputerItem = RPCitem.values()[RandomGen.nextInt(3)];
		
		//drop shadow setup
		EffectOne.setColor(Color.BLUE);
		EffectOne.setRadius(80);
		EffectTwo.setColor(Color.CRIMSON);
		EffectTwo.setRadius(80);
		EffectThree.setColor(Color.PURPLE);
		EffectThree.setRadius(80);
		EffectOneText.setColor(Color.BLUE);
		EffectOneText.setSpread(.75);
		EffectOneText.setRadius(35);
		EffectTwoText.setColor(Color.CRIMSON);
		EffectTwoText.setSpread(.75);
		EffectTwoText.setRadius(35);
		EffectThreeText.setColor(Color.PURPLE);
		EffectThreeText.setSpread(.75);
		EffectThreeText.setRadius(35);
		
		//setup images and GridPane
		imvItems.get(0).setFitWidth(200);
		imvItems.get(0).setPreserveRatio(true);
		imvItems.get(1).setFitWidth(200);
		imvItems.get(1).setPreserveRatio(true);
		imvItems.get(2).setFitHeight(250);
		imvItems.get(2).setPreserveRatio(true);
		GridPaneOne.setPadding(new Insets(15));
		GridPaneOne.setHgap(10.0);
		GridPaneOne.setVgap(15.0);
		
		GridPaneOne.add(imvItems.get(0), 1, 2);
		GridPaneOne.add(imvItems.get(1), 2, 1);
		GridPaneOne.add(imvItems.get(2), 3, 2);
		GridPaneOne.add(lblOutput, 1, 3, 3, 1);
		
		GridPane.setHalignment(lblOutput, HPos.CENTER);
	
		lblOutput.setStyle("-fx-font-weight: Bold;");
		
		//event handlers, click
		imvItems.get(0).setOnMousePressed(new imvRock_onMousePressed());
		imvItems.get(1).setOnMousePressed(new imvPaper_onMousePressed());
		imvItems.get(2).setOnMousePressed(new imvScissors_onMousePressed());
		imvItems.get(0).setOnMouseReleased(event -> {
			imvItems.get(0).setOpacity(1);
		});
		imvItems.get(1).setOnMouseReleased(event -> {
			imvItems.get(1).setOpacity(1);
		});
		imvItems.get(2).setOnMouseReleased(event -> {
			imvItems.get(2).setOpacity(1);
		});
		
		//set the scene, stage, and on with the show
		PrimaryStage.setTitle("Rock, Paper, Scissors");
		Scene PrimaryScene = new Scene(GridPaneOne);
		
		
		//PrimaryStage.setResizable(false);
		PrimaryStage.setMaxWidth(700);
		PrimaryStage.setMaxHeight(800);
		PrimaryStage.setScene(PrimaryScene);
		PrimaryStage.show();
		
	}
	
	class  imvRock_onMousePressed implements EventHandler<MouseEvent>{
		@Override
		public void handle(MouseEvent Event) {
			//reset drop shadows
			for(ImageView Item: imvItems) {
				Item.setEffect(null);
			}
			
			imvItems.get(0).setOpacity(.5);
			
			UserItem = RPCitem.ROCK;
			
			if(UserItem == ComputerItem) {
				lblOutput.setText("It's a Tie. The computer also chose " + ComputerItem.toString());
				//tie only has one drop shadow color
				lblOutput.setEffect(EffectThreeText);
				imvItems.get(0).setEffect(EffectThree);
			}
			else if(ComputerItem == RPCitem.SCISSORS) {
				lblOutput.setText("You Won! The computer chose " + ComputerItem.toString());
				//user win colors label with user color
				lblOutput.setEffect(EffectOneText);
				imvItems.get(ComputerItem.ordinal()).setEffect(EffectTwo);
				imvItems.get(0).setEffect(EffectOne);
			}
			else {
				lblOutput.setText("You Lost! The computer chose " + ComputerItem.toString());
				//computer win colors label with computer color
				lblOutput.setEffect(EffectTwoText);
				imvItems.get(ComputerItem.ordinal()).setEffect(EffectTwo);
				imvItems.get(0).setEffect(EffectOne);
			}
			
			ComputerItem = RPCitem.values()[RandomGen.nextInt(3)];
		}

	}
	
	class  imvPaper_onMousePressed implements EventHandler<MouseEvent>{
		@Override
		public void handle(MouseEvent Event) {
			for(ImageView Item: imvItems) {
				Item.setEffect(null);
			}
			
			imvItems.get(1).setOpacity(.5);
			
			UserItem = RPCitem.PAPER;
			
			if(UserItem == ComputerItem) {
				lblOutput.setText("It's a Tie. The computer also chose " + ComputerItem.toString());
				lblOutput.setEffect(EffectThreeText);
				imvItems.get(1).setEffect(EffectThree);
			}
			else if(ComputerItem == RPCitem.ROCK) {
				lblOutput.setText("You Won! The computer chose " + ComputerItem.toString());
				lblOutput.setEffect(EffectOneText);
				imvItems.get(ComputerItem.ordinal()).setEffect(EffectTwo);
				imvItems.get(1).setEffect(EffectOne);
			}
			else {
				lblOutput.setText("You Lost! The computer chose " + ComputerItem.toString());
				lblOutput.setEffect(EffectTwoText);
				imvItems.get(ComputerItem.ordinal()).setEffect(EffectTwo);
				imvItems.get(1).setEffect(EffectOne);
			}
			
			ComputerItem = RPCitem.values()[RandomGen.nextInt(3)];
		}
		
	}
	
	class  imvScissors_onMousePressed implements EventHandler<MouseEvent>{
		@Override
		public void handle(MouseEvent Event) {
			for(ImageView Item: imvItems) {
				Item.setEffect(null);
			}
			
			imvItems.get(2).setOpacity(.5);
			
			UserItem = RPCitem.SCISSORS;
			
			if(UserItem == ComputerItem) {
				lblOutput.setText("It's a Tie. The computer also chose " + ComputerItem.toString());
				lblOutput.setEffect(EffectThreeText);
				imvItems.get(2).setEffect(EffectThree);
			}
			else if(ComputerItem == RPCitem.PAPER) {
				lblOutput.setText("You Won! The computer chose " + ComputerItem.toString());
				lblOutput.setEffect(EffectOneText);
				imvItems.get(ComputerItem.ordinal()).setEffect(EffectTwo);
				imvItems.get(2).setEffect(EffectOne);
			}
			else {
				lblOutput.setText("You Lost! The computer chose " + ComputerItem.toString());
				lblOutput.setEffect(EffectTwoText);
				imvItems.get(ComputerItem.ordinal()).setEffect(EffectTwo);
				imvItems.get(2).setEffect(EffectOne);
			}

			ComputerItem = RPCitem.values()[RandomGen.nextInt(3)];
		}
	
	}
	
}
